CREATE DATABASE tugas_UTS_PPK;

USE tugas_UTS_PPK;

CREATE TABLE users(
    username         VARCHAR(100) NOT NULL ,
    password         VARCHAR(100) NOT NULL ,
    name             VARCHAR(100) NOT NULL ,
    token            VARCHAR(100),
    token_expired_at BIGINT,
    PRIMARY KEY (username),
    UNIQUE (token)
)ENGINE InnoDB;

SELECT * FROM users;

DESC users;

CREATE TABLE contacts(
    id          VARCHAR(100) NOT NULL ,
    username    VARCHAR(100) NOT NULL ,
    first_name  VARCHAR(100) NOT NULL ,
    last_name   VARCHAR(100),
    phone       VARCHAR(100),
    email       VARCHAR(100),
    PRIMARY KEY (id),
    FOREIGN KEY fk_users_contacts (username) REFERENCES users(username)
)ENGINE InnoDB;

SELECT * FROM contacts;

DESC contacts;

CREATE TABLE addresses(
  id                    VARCHAR(100) NOT NULL ,
  contact_id            VARCHAR(100) NOT NULL,
  Jalan                 VARCHAR(100),
  Kota                  VARCHAR(100),
  Provinsi              VARCHAR(100),
  Himada                VARCHAR(100) NOT NULL ,
  Jurusan               VARCHAR(100) NOT NULL ,
  Tahun_lulus           VARCHAR(100) NOT NULL ,
  Penempatan_pertama    VARCHAR(100) NOT NULL ,
  PRIMARY KEY (id),
  FOREIGN KEY fk_contacts_addresses (contact_id) REFERENCES contacts (id)
)ENGINE InnoDB;

SELECT * FROM addresses;

DESC addresses;
