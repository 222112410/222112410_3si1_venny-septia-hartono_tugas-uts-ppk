# Address API Spec

## Create Address

Endpoint : POST /api/contacts/{idContact}/addresses

Request Header :

- X-API-TOKEN : Token (Mandatory)

Request Body :
```json
{
  "Jalan" : "Jalan apa",
  "Kota"  : "Asal Kota mana",
  "Provinsi"  : "Asal Provinsi",
  "Himada"  : "Bekisar",
  "Jurusan"   : "ProgramStudi",
  "Tahun Lulus" : "Tahun",
  "Penempatan pertama" : "Kupang"
}
```
Response Body (Success) :
```json
{
  "data" : {
    "id" : "random-string",
    "Jalan" : "Jalan apa",
    "Kota"  : "Asal Kota mana",
    "Provinsi"  : "Asal Provinsi",
    "Himada"  : "Bekisar",
    "Jurusan"   : "ProgramStudi",
    "Tahun Lulus" : "Tahun",
    "Penempatan pertama" : "Kupang"
  }
}
```
Response Body (Failed) :

```json
{
  "error" : "Kontak tidak ditemukan" 
}
```

## Update Address

Endpoint : PUT /api/contacts/{idContact}/addresses/{idAddress}

Request Header :

- X-API-TOKEN : Token (Mandatory)

Request Body :
```json
{
  "Jalan" : "Jalan apa",
  "Kota"  : "Asal Kota mana",
  "Provinsi"  : "Asal Provinsi",
  "Jurusan"   : "ProgramStudi",
  "Himada"  : "Bekisar",
  "Tahun Lulus" : "Tahun",
  "Penempatan pertama" : "Kupang"
}
```
Response Body (Success) :
```json
{
  "data" : {
    "id" : "random-string",
    "Jalan" : "Jalan apa",
    "Kota"  : "Asal Kota mana",
    "Provinsi"  : "Asal Provinsi",
    "Himada"  : "Bekisar",
    "Jurusan"   : "ProgramStudi",
    "Tahun Lulus" : "Tahun",
    "Penempatan pertama" : "Kupang"
  }
}
```
Response Body (Failed) :

```json
{
  "error" : "Informasi Alamat tidak ditemukan" 
}
```

## Get Address

Endpoint : GET /api/contacts/{idContact}/addresses/{idAddress}

Request Header :

- X-API-TOKEN : Token (Mandatory)
  Request Body :
```json
{
  "Jalan" : "Jalan apa",
  "Kota"  : "Asal Kota mana",
  "Provinsi"  : "Asal Provinsi",
  "Himada"  : "Bekisar",
  "Jurusan"   : "ProgramStudi",
  "Tahun Lulus" : "Tahun",
  "Penempatan pertama" : "Kupang"
}
```
Response Body (Success) :
```json
{
  "data" : {
    "id" : "random-string",
    "Jalan" : "Jalan apa",
    "Kota"  : "Asal Kota mana",
    "Provinsi"  : "Asal Provinsi",
    "Himada"  : "Bekisar",
    "Jurusan"   : "ProgramStudi",
    "Tahun Lulus" : "Tahun",
    "Penempatan pertama" : "Kupang"
  }
}
```
Response Body (Failed) :

```json
{
  "error" : "Informasi Alamat tidak ditemukan" 
}
```

## Remove Address

Endpoint : DELETE /api/contacts/{idContact}/addresses/{idAddress}

Request Header :

- X-API-TOKEN : Token (Mandatory)

Response Body (Success) :
```json
{
  "data" : "OK" 
}
```
Response Body (Failed) :
```json
{
  "error" : "Informasi Alamat tidak ditemukan" 
}
```
## List Address

Endpoint : GET /api/contacts/{isContact}/addresses

Request Header :

- X-API-TOKEN : Token (Mandatory)

Response Body (Success) :
```json
{
  "data" : [
    {
      "id" : "random-string",
      "Jalan" : "Jalan apa",
      "Kota"  : "Asal Kota mana",
      "Provinsi"  : "Asal Provinsi",
      "Himada"  : "Bekisar",
      "Jurusan"   : "ProgramStudi",
      "Tahun Lulus" : "Tahun",
      "Penempatan pertama" : "Kupang"
    }
  ]
}
```
Response Body (Failed) :

```json
{
  "error" : "Kontak tidak ditemukan" 
}
```
