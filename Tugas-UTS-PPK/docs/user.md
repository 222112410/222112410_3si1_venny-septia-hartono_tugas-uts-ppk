# User API Spec

## Register User

Endpoint : POST /api/users

Request Body :

```json
{
    "username" : "VennySH",
    "password" : "Venny12345",
    "name"  : "Venny Septia Hartono"
}
```

Response Body (Success):

```json
{
    "data" : "Registrasi Berhasil"
}
```
Response Body (Failed):

```json
{
    "errors" : "Username tidak boleh kosong"
}
```

## Login User
Endpoint : POST /api/auth/login

Request Body :

```json
{
    "username" : "VennySH",
    "password" : "12345"
}
```

Response Body (Success):

```json
{
    "data" : {
      "token" : "TOKEN",
      "expiredAt" : 76373676786 // milliseconds
    }
}
```
Response Body (Failed):

```json
{
    "errors" : "Username atau password tidak cocok"
}
```
## Get User
Endpoint : GET/api/users/current

Request Header :
- X-API-TOKEN : Token (Mandatory)

Response Body (Success):

```json
{
    "data" : {
      "username" : "VennySH",
      "name"  : "Venny Septia Hartono"
    }
}
```
Response Body (Failed):

```json
{
    "errors" : "Unauthorized"
}
```

## Update User

Endpoint : PATCH/api/users/current

Request Header :

- X-API-TOKEN : Token (Mandatory)

Request Body :

```json
{
    "name"  : "Venny Septia",  // mengupdate tergantung dari data yang dikirim
    "password" : "12345baru"
}
```

Response Body (Success):

```json
{
    "data" : {
      "username" : "VennySH",
      "name"  : "Venny Septia Hartono"
    }
}
```
Response Body (Failed, 401):

```json
{
    "errors" : "Unauthorized"
}
```


## Logout User
Endpoint : DELETE/api/auth/logout

Request Header :

- X-API-TOKEN : Token (Mandatory)

Response Body (Succes):

```json
{
    "data" : "OK"
}
```

