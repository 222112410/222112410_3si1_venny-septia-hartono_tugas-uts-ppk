package bukualumni.restful;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TugasUtsPpkApplication {

	public static void main(String[] args) {
		SpringApplication.run(TugasUtsPpkApplication.class, args);
	}

}
