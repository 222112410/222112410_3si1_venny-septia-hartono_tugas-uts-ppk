package bukualumni.restful.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "addresess")
public class Address {
    @Id
    private String id;

    private String Jalan;

    private String Kota;

    private String Provinsi;

    private String Himada;

    private String Jurusan;

    @Column(name = "Tahun_lulus")
    private String Tahun_lulus;

    private String Penempatan_pertama;

    @ManyToOne
    @JoinColumn(name = "contact_id", referencedColumnName = "id")
    private Contact contact;
}
