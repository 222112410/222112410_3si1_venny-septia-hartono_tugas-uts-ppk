package bukualumni.restful.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddressResponse {
    private String id;

    private String Jalan;

    private String Kota;

    private String Provinsi;

    private String Himada;

    private String Jurusan;

    private String Tahun_lulus;

    private String Penempatan_pertama;

}
