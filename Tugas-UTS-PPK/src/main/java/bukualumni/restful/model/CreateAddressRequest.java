package bukualumni.restful.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateAddressRequest {

    @JsonIgnore
    @NotBlank
    private String contactId;

    @Size(max = 100)
    private String Jalan;

    @Size(max = 100)
    private String Kota;

    @Size(max = 100)
    private String Provinsi;

    @NotBlank
    @Size(max = 100)
    private String Himada;

    @NotBlank
    @Size(max = 100)
    private String Jurusan;

    @NotBlank
    @Size(max = 100)
    private String Tahun_lulus;

    @NotBlank
    @Size(max = 100)
    private String Penempatan_pertama;
}
