package bukualumni.restful.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import bukualumni.restful.entity.Address;
import bukualumni.restful.entity.Contact;
import bukualumni.restful.entity.User;
import bukualumni.restful.model.AddressResponse;
import bukualumni.restful.model.CreateAddressRequest;
import bukualumni.restful.model.UpdateAddressRequest;
import bukualumni.restful.repository.AddressRepository;
import bukualumni.restful.repository.ContactRepository;

import java.util.List;
import java.util.UUID;

@Service
public class AddressService {

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private ValidationService validationService;

    @Transactional
    public AddressResponse create(User user, CreateAddressRequest request) {
        validationService.validate(request);

        Contact contact = contactRepository.findFirstByUserAndId(user, request.getContactId())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Contact is not found"));

        Address address = new Address();
        address.setId(UUID.randomUUID().toString());
        address.setContact(contact);
        address.setJalan(request.getJalan());
        address.setKota(request.getKota());
        address.setProvinsi(request.getProvinsi());
        address.setHimada(request.getHimada());
        address.setJurusan(request.getJurusan());
        address.setTahun_lulus(request.getTahun_lulus());
        address.setPenempatan_pertama(request.getPenempatan_pertama());

        addressRepository.save(address);

        return toAddressResponse(address);
    }

    private AddressResponse toAddressResponse(Address address) {
        return AddressResponse.builder()
                .id(address.getId())
                .Jalan(address.getJalan())
                .Kota(address.getKota())
                .Provinsi(address.getProvinsi())
                .Himada(address.getHimada())
                .Jurusan(address.getJurusan())
                .Tahun_lulus(address.getTahun_lulus())
                .Penempatan_pertama(address.getPenempatan_pertama())
                .build();
    }

    @Transactional(readOnly = true)
    public AddressResponse get(User user, String contactId, String addressId) {
        Contact contact = contactRepository.findFirstByUserAndId(user, contactId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Kontak tidak ditemukan"));

        Address address = addressRepository.findFirstByContactAndId(contact, addressId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Alamat tidak ditemukan"));

        return toAddressResponse(address);
    }

    @Transactional
    public AddressResponse update(User user, UpdateAddressRequest request){
        validationService.validate(request);

        Contact contact = contactRepository.findFirstByUserAndId(user, request.getContactId())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Kontak tidak ditemukan"));

        Address address = addressRepository.findFirstByContactAndId(contact, request.getAddressId())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Alamat tidak ditemukan"));

        address.setJalan(request.getJalan());
        address.setKota(request.getKota());
        address.setProvinsi(request.getProvinsi());
        address.setHimada(request.getHimada());
        address.setJurusan(request.getJurusan());
        address.setTahun_lulus(request.getTahun_lulus());
        address.setPenempatan_pertama(request.getPenempatan_pertama());
        addressRepository.save(address);

        return toAddressResponse(address);
    }


    @Transactional
    public void remove(User user, String contactId, String addressId){
        Contact contact = contactRepository.findFirstByUserAndId(user, contactId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Kontak tidak ditemukan"));

        Address address = addressRepository.findFirstByContactAndId(contact, addressId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Alamat tidak ditemukan"));

        addressRepository.delete(address);
    }

    @Transactional(readOnly = true)
    public List<AddressResponse> list(User user, String contactId){
        Contact contact = contactRepository.findFirstByUserAndId(user, contactId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Kontak tidak ditemukan"));

        List<Address> addresses = addressRepository.findAllByContact(contact);
        return addresses.stream().map(this::toAddressResponse).toList();
    }
}
