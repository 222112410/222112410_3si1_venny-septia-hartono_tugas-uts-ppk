package bukualumni.restful.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import bukualumni.restful.entity.Address;
import bukualumni.restful.entity.Contact;
import bukualumni.restful.entity.User;
import bukualumni.restful.model.AddressResponse;
import bukualumni.restful.model.CreateAddressRequest;
import bukualumni.restful.model.UpdateAddressRequest;
import bukualumni.restful.model.WebResponse;
import bukualumni.restful.repository.AddressRepository;
import bukualumni.restful.repository.ContactRepository;
import bukualumni.restful.repository.UserRepository;
import bukualumni.restful.security.BCrypt;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.MockMvcBuilder.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class AddressControllerTest {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        addressRepository.deleteAll();
        contactRepository.deleteAll();
        userRepository.deleteAll();

        User user = new User();
        user.setUsername("test");
        user.setPassword(BCrypt.hashpw("test", BCrypt.gensalt()));
        user.setName("Test");
        user.setToken("test");
        user.setTokenExpiredAt(System.currentTimeMillis() + 1000000);
        userRepository.save(user);

        Contact contact = new Contact();
        contact.setId("test");
        contact.setUser(user);
        contact.setFirstName("Venny");
        contact.setLastName("Septia Hartono");
        contact.setEmail("222112410@stis.ac.id");
        contact.setPhone("085334347296");
        contactRepository.save(contact);
    }

    @Test
    void createAddressBadRequest() throws Exception {
        CreateAddressRequest request = new CreateAddressRequest();
        request.setKota("");

        mockMvc.perform(
                post("/api/contacts/test/addresses")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isBadRequest()
        ).andDo(result -> {
            WebResponse<String> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNotNull(response.getErrors());
        });
    }

    @Test
    void createAddressSuccess() throws Exception {
        CreateAddressRequest request = new CreateAddressRequest();
        request.setJalan("Jalan");
        request.setKota("Blitar");
        request.setProvinsi("Jawa Timur");
        request.setHimada("Bekisar");
        request.setJurusan("D-IV Komputasi Statistik");
        request.setTahun_lulus("2025");
        request.setPenempatan_pertama("Kupang");

        mockMvc.perform(
                post("/api/contacts/test/addresses")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<AddressResponse> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNull(response.getErrors());
            assertEquals(request.getJalan(), response.getData().getJalan());
            assertEquals(request.getKota(), response.getData().getKota());
            assertEquals(request.getProvinsi(), response.getData().getProvinsi());
            assertEquals(request.getHimada(), response.getData().getHimada());
            assertEquals(request.getJurusan(), response.getData().getJurusan());
            assertEquals(request.getTahun_lulus(), response.getData().getTahun_lulus());
            assertEquals(request.getPenempatan_pertama(), response.getData().getPenempatan_pertama());

            assertTrue(addressRepository.existsById(response.getData().getId()));
        });
    }

    @Test
    void getAddressNotFound() throws Exception {
        mockMvc.perform(
                get("/api/contacts/test/addresses/test")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isNotFound()
        ).andDo(result -> {
            WebResponse<String> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNotNull(response.getErrors());
        });
    }

    @Test
    void getAddressSuccess() throws Exception {
        Contact contact = contactRepository.findById("test").orElseThrow();

        Address address = new Address();
        address.setId("test");
        address.setContact(contact);
        address.setJalan("Jalan");
        address.setKota("Blitar");
        address.setProvinsi("Jawa Timur");
        address.setHimada("Bekisar");
        address.setJurusan("D-IV Komputasi Statistik");
        address.setTahun_lulus("2025");
        address.setPenempatan_pertama("Kupang");

        addressRepository.save(address);

        mockMvc.perform(
                get("/api/contacts/test/addresses/test")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<AddressResponse> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNull(response.getErrors());
            assertEquals(address.getId(), response.getData().getId());
            assertEquals(address.getJalan(), response.getData().getJalan());
            assertEquals(address.getKota(), response.getData().getKota());
            assertEquals(address.getProvinsi(), response.getData().getProvinsi());
            assertEquals(address.getHimada(), response.getData().getHimada());
            assertEquals(address.getJurusan(), response.getData().getJurusan());
            assertEquals(address.getTahun_lulus(), response.getData().getTahun_lulus());
            assertEquals(address.getPenempatan_pertama(), response.getData().getPenempatan_pertama());

        });
    }

    @Test
    void updateAddressBadRequest() throws Exception {
        UpdateAddressRequest request = new UpdateAddressRequest();
        request.setKota("");

        mockMvc.perform(
                put("/api/contacts/test/addresses/test")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isBadRequest()
        ).andDo(result -> {
            WebResponse<String> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNotNull(response.getErrors());
        });
    }

    @Test
    void updateAddressSuccess() throws Exception {
        Contact contact = contactRepository.findById("test").orElseThrow();

        Address address = new Address();
        address.setId("test");
        address.setContact(contact);
        address.setJalan("Lama");
        address.setKota("Lama");
        address.setProvinsi("Lama");
        address.setHimada("Lama");
        address.setJurusan("Lama");
        address.setTahun_lulus("Lama");
        address.setPenempatan_pertama("Lama");

        addressRepository.save(address);

        UpdateAddressRequest request = new UpdateAddressRequest();
        request.setJalan("Jalan");
        request.setKota("Blitar");
        request.setProvinsi("Jawa Timur");
        request.setHimada("Bekisar");
        request.setJurusan("D-IV Komputasi Statistik");
        request.setTahun_lulus("2025");
        request.setPenempatan_pertama("Kupang");

        mockMvc.perform(
                put("/api/contacts/test/addresses/test")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<AddressResponse> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNull(response.getErrors());
            assertEquals(request.getJalan(), response.getData().getJalan());
            assertEquals(request.getKota(), response.getData().getKota());
            assertEquals(request.getProvinsi(), response.getData().getProvinsi());
            assertEquals(request.getHimada(), response.getData().getHimada());
            assertEquals(request.getJurusan(), response.getData().getJurusan());
            assertEquals(request.getTahun_lulus(), response.getData().getTahun_lulus());
            assertEquals(request.getPenempatan_pertama(), response.getData().getPenempatan_pertama());

            assertTrue(addressRepository.existsById(response.getData().getId()));
        });
    }

    @Test
    void deleteAddressNotFound() throws Exception {
        mockMvc.perform(
                delete("/api/contacts/test/addresses/test")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isNotFound()
        ).andDo(result -> {
            WebResponse<String> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNotNull(response.getErrors());
        });
    }

    @Test
    void deleteAddressSuccess() throws Exception {
        Contact contact = contactRepository.findById("test").orElseThrow();

        Address address = new Address();
        address.setId("test");
        address.setContact(contact);
        address.setJalan("Jalan");
        address.setKota("Blitar");
        address.setProvinsi("Jawa Timur");
        address.setHimada("Bekisar");
        address.setJurusan("D-IV Komputasi Statistik");
        address.setTahun_lulus("2025");
        address.setPenempatan_pertama("Kupang");

        addressRepository.save(address);

        mockMvc.perform(
                delete("/api/contacts/test/addresses/test")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<String> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNull(response.getErrors());
            assertEquals("OK", response.getData());

            assertFalse(addressRepository.existsById("test"));
        });
    }

    @Test
    void listAddressNotFound() throws Exception {
        mockMvc.perform(
                get("/api/contacts/salah/addresses")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isNotFound()
        ).andDo(result -> {
            WebResponse<String> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNotNull(response.getErrors());
        });
    }

    @Test
    void listAddressSuccess() throws Exception {
        Contact contact = contactRepository.findById("test").orElseThrow();

        for (int i = 0; i < 5; i++) {
            Address address = new Address();
            address.setId("test-" + i);
            address.setContact(contact);
            address.setJalan("Jalan");
            address.setKota("Blitar");
            address.setProvinsi("Jawa Timur");
            address.setHimada("Bekisar");
            address.setJurusan("D-IV Komputasi Statistik");
            address.setTahun_lulus("2025");
            address.setPenempatan_pertama("Kupang");

            addressRepository.save(address);
        }

        mockMvc.perform(
                get("/api/contacts/test/addresses")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("X-API-TOKEN", "test")
        ).andExpectAll(
                status().isOk()
        ).andDo(result -> {
            WebResponse<List<AddressResponse>> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
            });
            assertNull(response.getErrors());
            assertEquals(5, response.getData().size());
        });
    }
}
